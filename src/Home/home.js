import React from "react";

function Home() {
  return (
    <div className="container" style={{ marginTop: "120px" }}>
      <div className="row justify-content-center">
        <div className="col-md-4">
          <div className="card border-0 rounded shadow-sm">
            <div className="card-body">Home</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
