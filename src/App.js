import "./App.css";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Dating from "./Dating/dating";
import Home from "./Home/home";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/dating" component={Dating} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
