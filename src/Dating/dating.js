import React, { useEffect, useState } from "react";
import database from "../firebase";
import { ref, push, get } from "firebase/database";
import Button from "react-bootstrap/Button";

import "./dating.css";
const Dating = () => {
  const [datingDestination, setDatingDestination] = useState({
    destination: "",
  });
  const [datingDatas, setDatingDatas] = useState([]);

  useEffect(() => {
    document.title = "Dating Destination";
    readData();
  }, []);

  async function readData() {
    const destinationsRef = ref(database, "/destinations");
    get(destinationsRef).then((destination) => {
      console.log(destination);
      const data = destination.val();
      setDatingDatas([]);
      for (var x in data) {
        // eslint-disable-next-line no-loop-func
        setDatingDatas((oldDatas) => [
          ...oldDatas,
          data[x].datingDestination.destination,
        ]);
      }
    });
  }

  function createData(val) {
    val.preventDefault();
    push(ref(database, "/destinations"), {
      datingDestination: datingDestination,
    }).then(() => {
      setDatingDestination({
        destination: "",
      });
      readData();
    });
  }

  const onChange = (e) => {
    setDatingDestination({
      ...datingDestination,
      error: false,
      [e.target.name]: e.target.value,
    });
  };

  const showResults = () => {
    let id = Math.floor(Math.random() * datingDatas.length);
    alert(datingDatas[id]);
  };

  return (
    <div>
      <h1>Dating Destination</h1>
      <div>
        <form onSubmit={createData}>
          <div className="form-group">
            <label htmlFor="destination">New Destination:</label>
            <input
              className="form-control"
              id="destination"
              type="text"
              name="destination"
              value={datingDestination["destination"]}
              required
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor=""></label>
          </div>
          <div className="form-group">
            <button className="form-control" type="submit" class="btn btn-dark">
              Add new destination
            </button>
          </div>
          <hr />
        </form>
      </div>
      <Button onClick={showResults} variant="primary">
        Get Random Destination
      </Button>
      <hr />
      <h2 className="text-white">Destination List: </h2>
      <ul class="dating-destination-list">
        {datingDatas.map((object, i) => (
          <li key={i}>{object}</li>
        ))}
      </ul>
    </div>
  );
};

export default Dating;
