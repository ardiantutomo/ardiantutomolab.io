import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyCf-H1o5uENcQCNbJYGsZtyqVHBIINsikM",
  authDomain: "portofolioardiant.firebaseapp.com",
  databaseURL:
    "https://portofolioardiant-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "portofolioardiant",
  storageBucket: "portofolioardiant.appspot.com",
  messagingSenderId: "832618007142",
  appId: "1:832618007142:web:4c5fd1450ce3effb647ba7",
  measurementId: "G-DT8ZP8F5WV",
};

const app = initializeApp(firebaseConfig);

// Get a reference to the database service
const database = getDatabase(app);
export default database;
